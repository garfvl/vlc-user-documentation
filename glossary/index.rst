.. _glossary:

############
  Glossary
############

This page lists definitions for terms used in VLC and this documentation.

.. glossary::
   :sorted:

   Stereo
        This is the reproduction of the sound in two or more independent audio channels using more than one speaker. If you use this option, you would feel as though the sound is played from all the directions. You can observe this in a regular home theatre with 5.1 or 6.1 speakers.

   Mono
        A monaural sound that uses a single channel for sound reproduction.

   Left
        You can observe this in a regular audio player with 2.1 speakers. If you select the Left option, the music is played only in the left speaker. The speaker on your right is automatically switched OFF.

   Right
        If you select the Right option, the music is played only in the speaker on your right side. The speaker on your left is automatically switched OFF.

   Reverse Stereo
        There are several applications that are used to reverse the stereo whereas VLC has an in-built feature to reverse the stereo. This option is useful if you want the audio to play in tandem with the video. You can use the Reverse Stereo option if you want to deliberately change the audio output.
        Imagine that you are watching a video. In the video, a person walks on the left side but the sound is produced on the right speaker. You can correct this by selecting the Reverse Stereo option in VLC. Select the Reverse Stereo option and play the same scene in the video and observe the difference.

   Unicast 
        This is a process where media files are sent to a single system through the network. 

   Multicast
        This is a process where media files are sent to multiple systems through the network.