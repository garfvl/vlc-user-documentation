.. _playback_controls:

Playback Controls
=================

Here, you have access to many functions to control the playback of a media file.

.. figure::  /static/images/interface/playbackcontrols_windows.png
   :align:   center